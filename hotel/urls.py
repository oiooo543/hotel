from django.urls import path

from hotel import views, specialviews

urlpatterns = [
        path('hotel/', views.HotelList.as_view()),
        path('hotel/detail/<int:pk>/', views.HotelDetail.as_view()),
        path('room-type/', views.RoomTypeList.as_view()),
        path('room-type/<int:pk>/', views.RoomTypeDetail.as_view()),
        path('room/', views.RoomList.as_view()),
        path('room/<int:pk>', views.RoomDetail.as_view()),
        path('rooms', specialviews.HotelRoomList.as_view()),
        path('room-allocate/', views.Reservation.as_view()),
        path('roomallocations', specialviews.RoomReservationsList.as_view()),
        path('checkdate/', specialviews.CheckDateAvaliability.as_view())
    ]