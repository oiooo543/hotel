from rest_framework import serializers

from hotel.models import Hotel, Room, Reservation, RoomType


class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = '__all__'


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = '__all__'


class RoomTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomType
        fields = '__all__'


class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = '__all__'
        read_only_fields = ['user']


class HotelRoomSerializer(serializers.ModelSerializer):
    class Metal:
        model = Room
        fields = ['name', 'type',  'current_price', 'normal_price', 'hotel']


