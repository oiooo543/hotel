from datetime import datetime

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

from hotel.hotelserializer import RoomSerializer, ReservationSerializer
from hotel.models import Room, Reservation


class HotelRoomList(generics.ListAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['hotel']


class RoomReservationsList(generics.ListAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['room']


class CheckDateAvaliability(APIView):
    def post(self, request, format=None):
        serializer = ReservationSerializer(data=request.data)

        if serializer.is_valid():
            start_date = serializer.data.get('start_date')
            end_date = serializer.data.get('end_date')
            hotel = serializer.data.get('hotel')
            reservations = Reservation.objects.filter(start_date__gte=start_date, end_date__lte=end_date, hotel=hotel)
            if reservations:
                return Response("room not available", status=status.HTTP_400_BAD_REQUEST)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response("invalid data", status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, format=None):
        queryset = Reservation.objects.filter(end_date__lte=datetime.now())
        serializer_class = ReservationSerializer(queryset)
        if queryset:
            return Response(serializer_class.data, status=status.HTTP_200_OK)