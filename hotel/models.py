# Create your models here.
from django.db import models

from demohotel import settings


class Hotel(models.Model):
    name = models.CharField(null=False, max_length=100)
    address = models.CharField(max_length=200)
    logitude = models.DecimalField(max_digits=7, decimal_places=5)
    latitude = models.DecimalField(max_digits=7, decimal_places=5)
    stars = models.IntegerField()


class RoomType(models.Model):
    name = models.CharField(max_length=100)


class Room(models.Model):
    name = models.CharField(max_length=100)
    type = models.ForeignKey(RoomType, on_delete=models.DO_NOTHING)
    normal_price = models.DecimalField(max_digits=4, decimal_places=3)
    current_price = models.DecimalField(max_digits=4, decimal_places=3)
    status = models.BooleanField(default=True)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)


class Reservation(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.DO_NOTHING)
    room = models.ForeignKey(Room, on_delete=models.DO_NOTHING)
    reservation_price = models.DecimalField(max_digits=4, decimal_places=3)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    total_cost = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.DO_NOTHING)