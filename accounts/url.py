from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from accounts import views

urlpatterns = [
    path('user/', views.UserList.as_view()),
    path('detail/<int:pk>/', views.UserDetail.as_view(), name='user-detail'),
    path('bio/', views.BioList.as_view()),
    path('bio/detail/<int:pk>/', views.BioDetail.as_view()),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
