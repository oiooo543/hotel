from time import sleep

from celery import shared_task
from django.core.mail import send_mail


@shared_task
def sleepy(duration):
    sleep(duration)
    return None

@shared_task
def send_email_task():
    send_mail('This is testing queues', 'This is proof the task is working', 'demo@demo.com', ['tofem97739@dmail1.net'])
    return None
