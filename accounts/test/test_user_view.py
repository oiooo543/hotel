from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from accounts.models import User


class UserListViewTest(APITestCase):
    @classmethod
    def setUpTestData(self):

        for user_id in range(15):
            User.objects.create(first_name='oloyede', last_name='oladimeji', username='olad' + user_id.__str__(),
                                gender='male', dob='2019-10-6', phone='08132193617', email='test@test.com')


    def test_get_all_users(self):
        response = self.client.get('/account/user/')
        self.assert_(len(response.data) > 1)
        self.assertEquals(response.status_code, 200)

    def test_get_one_user(self):
        request = reverse('user-detail', kwargs={'pk':1})
        response = self.client.get(request)

        self.assertEquals(response.status_code, 200)
        print(response.data)
