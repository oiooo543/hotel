from django.test import TestCase

from accounts.models import User


class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(first_name='oloyede', last_name='oladimeji', gender='male', dob='2019-10-6',
                                   phone='08132193617', email='test@test.com')

    def test_lenght_of_data(self):
        user = User.objects.get(id=1)
        size = len(user.first_name)
        self.assert_(size<100)
