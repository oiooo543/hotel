# Create your views here.
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.authentication import JWTAuthentication

from accounts.models import User, Bio
from accounts.permissions import IsOwnerOrReadOnly
from accounts.task import send_email_task
from accounts.user_serializer import UserSerializer, BioSerializer


class UserList(generics.ListCreateAPIView):
    permission_classes = [AllowAny,]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    send_email_task().delay()


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny, ]
    queryset = User.objects.all()
    serializer_class = UserSerializer


class BioList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication,]
    queryset = Bio.objects.all()
    serializer_class = BioSerializer


class BioDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsOwnerOrReadOnly]
    queryset = Bio.objects.all()
    serializer_class = BioSerializer

