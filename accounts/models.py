from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from demohotel import settings


def upload_dp(instance, filename):
    return "account/{user}/{filename}".format(user=instance.user, filename=filename)

class User(AbstractUser):
    gender = models.CharField(max_length=100)
    dob = models.DateField()
    phone = models.CharField(max_length=100)




class Bio(models.Model):
    pob = models.CharField(max_length=100)
    img = models.ImageField(upload_to=upload_dp, null=True)
    color = models.CharField(max_length=100)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)