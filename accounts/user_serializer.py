from rest_framework import serializers

from accounts.models import User, Bio


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password', 'first_name', 'is_active', 'last_name', 'dob', 'gender', 'phone', 'email']

        extra_kwargs = {'password': {'write_only': True}, 'is_active': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        print(password)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
            print(instance.set_password(password))
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class BioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bio
        fields = ['pob', 'img', 'color', 'user']
        read_only_fields = ['user']

